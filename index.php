<?php

require('Animal.php');
require('Ape.php'); 
require('Frog.php');

$sheep = new Animal("shaun");

echo "Aku punya  binatang bernama= ". $sheep->name . "<br>";
echo "jumlah kakinya = ". $sheep->legs . "<br>";
echo "dia berdarah dingin? = ". $sheep->cold_blooded . "<br>";


$sungokong = new Ape("kera sakti");
echo "Aku punya  binatang bernama= ". $sungokong->name . "<br>";
echo "jumlah kakinya = ". $sungokong->legs . "<br>";
echo "dia berdarah dingin? = ". $sungokong->cold_blooded . "<br>"; 
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "Aku punya  binatang bernama= ". $kodok->name . "<br>";
echo "jumlah kakinya = ". $kodok->legs . "<br>";
echo "dia berdarah dingin? = ". $kodok->cold_blooded . "<br>";
$kodok->jump() ; // "hop hop"
 



?>

